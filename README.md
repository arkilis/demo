## What is in the project

* Full functional code written in swift 3
* Unit Test for ViewModel and Network API

----
## Screenshot

![http://www.arkilis.me/wp-content/uploads/2017/09/tatts.png](http://www.arkilis.me/wp-content/uploads/2017/09/tatts.png)


## Third party libraries

This project uses `Carthage` to manage all third party libraries, the following is a manfiest for all in use.

```bash
github "Alamofire/Alamofire" ~> 4.4
github "SnapKit/SnapKit" ~> 3.2.0
github "Krimpedance/KRProgressHUD"
github "SwiftyJSON/SwiftyJSON"
github "onevcat/Kingfisher" ~> 3.0
```
----
## More about myself

Fore more information, you can find me:

* [Web](http://arkilis.me)
* [Medium](https://medium.com/@arkilis)
* [Twitter](https://twitter.com/arkilis) 

Thanks


