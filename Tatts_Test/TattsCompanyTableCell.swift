//
//  TattsCompanyTableCell.swift
//  Tatts_Test
//
//  Created by Ben on 11/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher


class TattsCompanyTableCell: UITableViewCell {
    
    
    var imgLogoImage: UIImageView!
    var labelCompanyDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.imgLogoImage = UIImageView()
        self.labelCompanyDesc = UILabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createLayout(szLogoURL: String, szCompanyDesc: String){
        let viewContent = self.contentView
        
        // Logo image
        let url = URL(string: szLogoURL)
        imgLogoImage.contentMode = .scaleAspectFit
        imgLogoImage.kf.setImage(with: url)
        viewContent.addSubview(imgLogoImage)
        imgLogoImage.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(viewContent.snp.left).offset(20)
            make.centerY.equalTo(viewContent.snp.centerY)
            make.height.equalTo(80)
            make.width.equalTo(80)
        }
        
        // company desc label
        labelCompanyDesc.font = UIFont(name:"HelveticaNeue", size: 14)
        labelCompanyDesc.text = "\(szCompanyDesc)"
        //labelCompanyDesc.textColor = UIColor.init(hex: g_color.ORANGECOLORHEX)
        labelCompanyDesc.textAlignment = .left
        viewContent.addSubview(labelCompanyDesc)
        labelCompanyDesc.snp.makeConstraints { (make) -> Void in
            make.left.equalTo(imgLogoImage.snp.right).offset(8)
            make.centerY.equalTo(viewContent.snp.centerY)
            make.height.equalTo(20)
            make.width.equalTo(viewContent.snp.width).multipliedBy(0.8)
        }
    }
}
