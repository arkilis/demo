//
//  CompanyTableViewModelTests.swift
//  CompanyTableViewModelTests
//
//  Created by Ben on 12/9/17.
//  Copyright © 2017 Yang. All rights reserved.
//

import XCTest

@testable import Tatts_Test

class CompanyTableViewModelTests: XCTestCase {
    
    
    let c = CompanyTableViewModel()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCompanyTableViewModelNumberOfSections(){
        
        XCTAssertEqual(1, c.numberOfSections())
    }
    
    func testCompanyTableViewModelNumberOfRowsInSection(){
        //let c = CompanyTableViewModel()
        XCTAssertEqual(0+1, c.numberOfRowsInSection())
    }
    
    func testCompanyTableViewModelHeightForRowAtZero(){
        //let c = CompanyTableViewModel()
        let indexPath = IndexPath(row:0, section:0)
        XCTAssertEqual(44, c.heightForRowAt(heightForRowAt: indexPath))
    }
    
    func testCompanyTableViewModelHeightForRowAtNotZero(){
        //let c = CompanyTableViewModel()
        let indexPath = IndexPath(row:1, section:0)
        XCTAssertEqual(80, c.heightForRowAt(heightForRowAt: indexPath))
    }
    
    func testCompanyTableViewModelGetData(){
        self.c.getData {
            XCTAssertEqual(5, self.c.aryResults.count)
        }
    }
    
    
    
}
